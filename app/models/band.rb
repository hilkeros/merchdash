class Band < ActiveRecord::Base

	#attr_accessible :name, :user_id, :events_attributes

	belongs_to :user

	has_many :events, :dependent => :destroy
  accepts_nested_attributes_for :events, :reject_if => proc { |a| a[:place].blank? }, :allow_destroy => true

end
