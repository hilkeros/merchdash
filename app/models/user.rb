class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :bands, :dependent => :destroy
  accepts_nested_attributes_for :bands, :reject_if => lambda { |a| a[:content].blank? }, :allow_destroy => true

end
