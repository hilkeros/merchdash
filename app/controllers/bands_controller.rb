class BandsController < InheritedResources::Base
	before_filter :authenticate_user!

	def new
		@user = current_user
		@band = Band.new
		event = @band.events.build
	end

	def create
		# @band = current_user.bands.create(params[:band])
		@band = current_user.bands.create(permitted_params)
		if @band.save
			redirect_to [@band]
		else
			render :edit
		end
	end
  
  def update
    @band = current_user.bands.find(params[:id])
    if(@band.update_attributes(permitted_params))
      redirect_to @band
    else
      render :edit
    end
  end
  
	def show
		@band = Band.find(params[:id])
		event = @band.events.build
	end

	private

	def permitted_params
    params.require(:band).permit(:name, :user_id, events_attributes: [:id, :date, :place, :band_id])
	end
end
