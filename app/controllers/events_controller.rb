class EventsController < InheritedResources::Base

	def permitted_params
		params.require(:event).permit(:date, :place, :band_id)
	end
end
