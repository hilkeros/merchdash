class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.date :date
      t.string :place
      t.integer :band_id

      t.timestamps
    end
  end
end
